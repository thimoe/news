import Vue from 'vue';

import hello from '../../components/cards/example1/example1.vue';

new Vue({
  el: '#app',
  components: {
    'hello': hello
  }
});