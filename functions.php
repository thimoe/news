<?php 

add_theme_support( 'title-tag' );


add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
    );
    
function my_styles(){

    wp_enqueue_style( 'style.min.css', get_template_directory_uri() . '/assets/css/style.min.css', null, '1.0' );
};
add_action('wp_enqueue_scripts', 'my_styles');

function my_scripts(){
    wp_enqueue_script('vue','https://cdn.jsdelivr.net/npm/vue/dist/vue.js');
    wp_enqueue_script('script.min.js', get_template_directory_uri() . '/assets/js/script.min.js', 'vue', '1.0' ,true);
}
add_action('wp_enqueue_scripts','my_scripts');



?>